"use strict";

const employee = {
  name: "Vitalii",
  surname: "Klichko",
};

const newEmployee = { ...employee, age: 30, salary: 10000 };

console.log(newEmployee);
