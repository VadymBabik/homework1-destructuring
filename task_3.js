"use strict";

const user1 = {
  name: "John",
  years: 30,
};

const { name, years: age, isAdmin = false } = user1;

const container = document.querySelector(".container");

const createElemUser = (e) => {
  const p = document.createElement("p");
  p.innerHTML = e;
  container.append(p);
};

createElemUser(name);
createElemUser(age);
createElemUser(isAdmin);
